import os


def encontrar_string(path,string):
    with open(path,'r') as f:
        texto=f.readlines()
    for i in texto:
        if string in i:
            return texto.index(i)
    print('String não encontrada')


def alterar_linha(path,linha_atual, nova_linha):
    index_linha = encontrar_string(path,linha_atual) 
    with open(path,'r') as f:
        texto=f.readlines()
    with open(path,'w') as f:
        for i in texto:
            if texto.index(i)==index_linha:
                f.write(nova_linha+'\n')
            else:
                f.write(i)

def main():
    path = "~/apks_python3/.env/lib/python3.6/site-packages/buildozer/targets/android.py"
    path = os.path.expanduser(path)
    alterar_linha(path, "if is_gradle_build:", "        if not is_gradle_build:")

if __name__ == '__main__':
    main()
